import HeroPage from "@/components/HeroPage"
import FooterSection from "@/sections/FooterSection"
import { Button, Container, Typography } from "@mui/material"
import Head from "next/head"
import Link from "next/link"
import React, { type FC } from "react"
import NavigationBar from "../components/NavigationBar"

const Home: FC = () => {
  return (
    <>
      <Head>
        <title>Fredericksburg BBQ Cleaning | Not found</title>
        <meta name="description" content="Extend the life of your grill in Fredericksburg" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <NavigationBar />
        <Container>
          <HeroPage
            py={5}
            image="/imgs/fredericksburg-bbq-chef-grilling.jpg"
            alt="Grill cleaning and maintenance"
          >
            <Typography variant="h2">
              Page not found
            </Typography>
            <Typography variant="body1" mt={2}>
              Unfortunately we could not find the page you&apos;re looking for.
            </Typography>
            <Link href="/#">
              <Button
                sx={{
                  mt: 2
                }}
                variant="contained"
              >
                Go home
              </Button>
            </Link>
          </HeroPage>
        </Container>

      </main>
      <footer>
        <FooterSection />
      </footer>
    </>
  )
}

export default Home
