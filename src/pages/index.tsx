import AboutSection from "@/sections/AboutSection"
import ContactSection from "@/sections/ContactSection"
import FAQSection from "@/sections/FAQSection"
import FooterSection from "@/sections/FooterSection"
import PricingSection from "@/sections/PricingSection"
import WorkSection from "@/sections/WorkSection"
import Head from "next/head"
import React, { type FC } from "react"
import NavigationBar from "../components/NavigationBar"
import LandingSection from "../sections/LandingSection"
import ServicesSection from "../sections/ServicesSection"

const Home: FC = () => {
  return (
    <>
      <Head>
        <title>Fredericksburg BBQ Cleaning</title>
        <meta name="description" content="Extend the life of your grill in Fredericksburg" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <NavigationBar />
        <LandingSection />
        <ServicesSection />
        <WorkSection />
        <PricingSection />
        <AboutSection />
        <FAQSection />
        <ContactSection />
      </main>
      <footer>
        <FooterSection />
      </footer>
    </>
  )
}

export default Home
