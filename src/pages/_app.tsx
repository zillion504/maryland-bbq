import { theme } from "../theme"
import { CssBaseline, ThemeProvider } from "@mui/material"
import type { AppProps } from "next/app"
import React from "react"
import { type FC } from "react"
import "../style.css"
import createEmotionCache from "@/createEmotionCache"
import { CacheProvider, type EmotionCache } from "@emotion/react"
import Head from "next/head"
import { Analytics } from "@vercel/analytics/react"

const clientSideEmotionCache = createEmotionCache()

interface MyAppProps extends AppProps {
  emotionCache?: EmotionCache
}

const App: FC<MyAppProps> = ({ Component, pageProps, emotionCache = clientSideEmotionCache }) => {
  return (
    <CacheProvider value={emotionCache}>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Component {...pageProps} />
        <Analytics />
      </ThemeProvider>
    </CacheProvider>
  )
}

export default App
