import { Box, Container, Grid, Typography } from "@mui/material"
import React from "react"
import { type FC } from "react"
import StyledCard from "../components/StyledCard"

const ServicesSection: FC = () => {
  return (
    <Box
      id="services"
      sx={{
        py: 10,
        backgroundColor: theme => theme.palette.background.paper
      }}
    >
      <Container maxWidth="lg">
        <Typography variant="h2" textAlign="center">Our Services</Typography>
        <Typography variant="body1" textAlign="center" mb={10}>We offer excellent grill cleaning, repair, and inspection services.</Typography>
        <Grid container spacing={2}>
          <Grid item md={4} xs={12}>
            <StyledCard
              sx={{
                bgcolor: theme => theme.palette.background.default
              }}
              src="/imgs/fredericksburg-bbq-cleaning.jpg"
              alt="Grill cleaning"
              title="Grill Cleaning"
              text="Eliminate all grime and carbon buildup. We'll leave your grill looking brand new!"
            />
          </Grid>
          <Grid item md={4} xs={12}>
            <StyledCard
              sx={{
                bgcolor: theme => theme.palette.background.default
              }}
              src="/imgs/grillfire.jpg"
              alt="Grill repair"
              title="Barbecue Repair"
              text="Need an expert to fix your grill? Our technicians are experienced and ready to help."
            />
          </Grid>
          <Grid item md={4} xs={12}>
            <StyledCard
              sx={{
                bgcolor: theme => theme.palette.background.default
              }}
              src="/imgs/fredericksburg-bbq-food-grilled.jpg"
              alt="Grill cleaning"
              title="BBQ Inspection"
              text="Unsure about your grill’s condition? We’ll send an expert out to look at it for you!"
            />
          </Grid>
        </Grid>
      </Container>
    </Box>
  )
}
export default ServicesSection
