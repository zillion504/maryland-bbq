import { Container, Typography } from "@mui/material"
import React from "react"
import { type FC } from "react"
import HeroPage from "../components/HeroPage"

const AboutSection: FC = () => {
  return (
    <Container
      id="about"
      maxWidth="lg"
    >
      <HeroPage
        py={5}
        image="/imgs/fredericksburg-grill-scrubbing.jpg"
        alt="Grill cleaning and maintenance"
      >
        <Typography variant="h2">
          About us
        </Typography>
        <Typography variant="body1" mt={2}>
          {"We're a local company that cleans and maintains grills all around Fredericksburg. Our mission is to make your grilling easy and hassle-free."}
        </Typography>
      </HeroPage>
    </Container>
  )
}
export default AboutSection
