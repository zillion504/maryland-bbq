import { Button, Container, Typography } from "@mui/material"
import Link from "next/link"
import React from "react"
import { type FC } from "react"
import HeroPage from "../components/HeroPage"

const LandingSection: FC = () => {
  return (
    <Container
      maxWidth="lg"
    >
      <HeroPage
        py={5}
        image="/imgs/grill-1.png"
        alt="Grill cleaning and maintenance"
      >
        <Typography variant="h2">
          Extend the life of your grill
        </Typography>
        <Typography variant="body1" mt={2}>
          Your grill deserves some love. We specialize in cleaning and repairing your grill so you don’t have to.
        </Typography>
        <Link href="#services">
          <Button
            sx={{
              mt: 2
            }}
            variant="contained"
          >
            View our services
          </Button>
        </Link>
      </HeroPage>
    </Container>
  )
}
export default LandingSection
