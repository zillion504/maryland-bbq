import StyledLink from "@/components/StyledLink"
import StyledNavLink from "@/components/StyledNavLink"
import { Container, Grid, Stack, Typography } from "@mui/material"
import { Box } from "@mui/system"
import { type FC } from "react"

const FooterSection: FC = () => {
  return (
    <Box
      sx={{
        py: 8,
        backgroundColor: theme => theme.palette.background.paper
      }}
    >
      <Container
        maxWidth="lg"
      >
        <Grid container rowSpacing={4}>
          <Grid item md={4} xs={12}>
            <Typography variant="h3" color="primary">Fredericksburg BBQ Cleaning</Typography>
          </Grid>
          <Grid item md={2} xs={4}>
            <Stack
              spacing={2}
            >
              <StyledNavLink href="/#">
                Home
              </StyledNavLink>
              <StyledNavLink href="/#services">
                Services
              </StyledNavLink>
              <StyledNavLink href="/#work">
                Work
              </StyledNavLink>
            </Stack>
          </Grid>
          <Grid item md={2} xs={4}>
            <Stack
              spacing={2}
            >
              <StyledNavLink href="/#pricing">
                Pricing
              </StyledNavLink>
              <StyledNavLink href="/#about">
                About
              </StyledNavLink>
              <StyledNavLink href="/#contact">
                Contact
              </StyledNavLink>
            </Stack>
          </Grid>
          <Grid item xs={4}>
            <Stack
              textAlign="right"
              spacing={2}
            >
              <Typography variant="body1">
                Designed by <StyledLink href="https://bgodley.com">Braden Godley</StyledLink>
              </Typography>
              <Typography variant="body1">
                Powered by <StyledLink href="https://webstrata.com">WebStrata</StyledLink>
              </Typography>
            </Stack>
          </Grid>
        </Grid>
      </Container>
    </Box>
  )
}
export default FooterSection
