import { Box, Button, Container, Link, Stack, Typography } from "@mui/material"
import React from "react"
import { type FC } from "react"
import HeroPage from "../components/HeroPage"

const FAQSection: FC = () => {
  return (<Box
    id="faq"
    sx={{
      backgroundColor: theme => theme.palette.background.paper
    }}
  >

    <Container
      maxWidth="lg"
      sx={{
        py: 10
      }}
    >
      <Typography variant="h2" textAlign="center">Frequently Asked Questions</Typography>
      <Typography variant="body1" textAlign="center">Got any questions? We can help.</Typography>

      <Stack
        spacing={8}
        py={10}
        sx={{
          px: {
            xs: 2,
            md: 0
          }
        }}
      >
        <Box>
          <Typography variant="h3">How do I tell how many burners I have?</Typography>
          <Typography variant="body1">
            The easiest way is to count the knobs on the front and side of the grill.
            We strive to clean your grill from top to bottom and from side to side. We will clean the entire grill and all of its components, from burners to smoker boxes (for smoking wood chips).
            You will know the price of our grill cleaning service prior to our starting on your grill.
          </Typography>
        </Box>
        <Box>
          <Typography variant="h3">Why is there a difference in price between free standing grills and built-in grills?</Typography>
          <Typography variant="body1">
            From our experience, built-in grills are made of heavier gauge stainless steel than most free standing grills.
            From the heavier hood to the thicker cooking grates, built-in grills take longer to clean in order to meet our standards.
          </Typography>
        </Box>
        <Box>
          <Typography variant="h3">How do you price repairs?</Typography>
          <Typography variant="body1">
            Prior to any repair or replacement parts being ordered and/ or installed, you will receive our total price in writing.
            In most cases, a second trip will be required so a minor trip charge will be added to the price of the repair.
            Again, you will know all fees prior to our doing the work.  Special order parts must be paid in advance.
          </Typography>
        </Box>
      </Stack>

      <HeroPage
        reverse
        imageHiddenMobile
        image="/imgs/fredericksburg-grill-degreasing.jpg"
        alt="Grill cleaning frequently asked questions"
      >
        <Typography variant="h2">
          Still have questions?
        </Typography>
        <Typography variant="body1" mt={2}>
          {"We'd love to talk to you about your specific needs. Please get in touch with us and we can help you with your grill cleaning or repairs."}
        </Typography>
        <Link href="#contact">
          <Button
            sx={{
              mt: 2
            }}
            variant="contained"
          >
            Contact us
          </Button>
        </Link>
      </HeroPage>
    </Container>
  </Box>
  )
}
export default FAQSection
