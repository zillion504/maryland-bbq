import { Box, Container, Grid, Hidden, Stack, Typography } from "@mui/material"
import Image from "next/image"
import React, { useState } from "react"
import { type FC } from "react"

type BeforeAfterState = "before" | "after"

const BeforeAfterButton: FC<{
  viewState: BeforeAfterState
  onClick: () => void
}> = ({ viewState, onClick }) => {
  return <Box
    sx={{
      borderColor: theme => theme.palette.secondary.main,
      borderRadius: "100px",
      border: theme => `4px solid ${theme.palette.secondary.main}`,
      position: "relative",
      mb: 10,
      cursor: "pointer"
    }}
    onClick={onClick}
  >
    <Stack
      sx={{
        mx: 2,
        my: 1
      }}
      spacing={4}
      direction="row"
    >
      <Typography
        sx={{
          zIndex: 2,
          width: "60px",
          textAlign: "center",
          color: theme => viewState === "before" ? theme.palette.getContrastText(theme.palette.primary.main) : theme.palette.getContrastText(theme.palette.background.default),
          transition: "all .3s"
        }}
      >
        Before
      </Typography>
      <Typography
        sx={{
          zIndex: 2,
          width: "60px",
          textAlign: "center",
          color: theme => viewState === "after" ? theme.palette.getContrastText(theme.palette.primary.main) : theme.palette.getContrastText(theme.palette.background.default),
          transition: "all .3s"
        }}
      >
        After
      </Typography>
    </Stack>
    <Box
      sx={{
        zIndex: 1,
        position: "absolute",
        borderRadius: "100px",
        backgroundColor: theme => theme.palette.primary.main,
        right: viewState !== "before" ? 0 : "50%",
        left: viewState === "before" ? 0 : "50%",
        top: 0,
        bottom: 0,
        transition: "all .3s"
      }}
    />
  </Box>
}

const BeforeAfterCard: FC<{
  beforeImg: string
  afterImg: string
  viewState: BeforeAfterState
}> = ({ viewState, beforeImg, afterImg }) => {
  return <Box
      sx={{
        overflow: "hidden",
        position: "relative",
        width: "100%",
        aspectRatio: "16 / 9",
        backgroundColor: theme => theme.palette.primary.main
      }}
    >
    <Box
      sx={{
        position: "absolute",
        width: "100%",
        height: "100%",
        left: viewState === "before" ? "0%" : "-100%",
        transition: "all .3s"
      }}
    >
      <Image
        alt="A dirty barbecue grill"
        style={{
          objectFit: "cover"
        }}
        fill
        src={beforeImg}
      />
    </Box>
    <Box
      sx={{
        position: "absolute",
        width: "100%",
        height: "100%",
        left: viewState === "before" ? "100%" : "0%",
        transition: "all .3s"
      }}
    >
      <Image
        alt="A dirty barbecue grill"
        fill
        style={{
          objectFit: "cover"
        }}
        src={afterImg}
      />
    </Box>
  </Box>
}

const WorkSection: FC = () => {
  const [viewState, setViewState] = useState<"before" | "after">("before")

  return (
    <Box
      id="work"
      sx={{
        py: 10
      }}
    >
      <Container maxWidth="lg">
        <Typography variant="h2" textAlign="center">Our Work</Typography>
        <Typography variant="body1" textAlign="center" mb={2}>We make your grill look as good as new.</Typography>
        <Box
          sx={{
            display: "flex",
            justifyContent: "center"
          }}
        >
          <BeforeAfterButton viewState={viewState} onClick={() => { setViewState(viewState === "before" ? "after" : "before") }} />
        </Box>
        <Grid container spacing={2}>
          <Grid item md={4} xs={6}>
            <BeforeAfterCard
              viewState={viewState}
              beforeImg={"/imgs/before-after/grill-builtin-1-before.jpg"}
              afterImg={"/imgs/before-after/grill-builtin-1-after.jpg"}
            />
          </Grid>
          <Grid item md={4} xs={6}>
            <BeforeAfterCard
              viewState={viewState}
              beforeImg={"/imgs/before-after/grill-double-1-before.jpg"}
              afterImg={"/imgs/before-after/grill-double-1-after.jpg"}
            />
          </Grid>
          <Grid item md={4} xs={6}>
            <BeforeAfterCard
              viewState={viewState}
              beforeImg={"/imgs/before-after/grill-double-2-before.jpg"}
              afterImg={"/imgs/before-after/grill-double-2-after.jpg"}
            />
          </Grid>
          <Grid item md={4} xs={6}>
            <BeforeAfterCard
              viewState={viewState}
              beforeImg={"/imgs/before-after/grill-single-1-before.jpg"}
              afterImg={"/imgs/before-after/grill-single-1-after.jpg"}
            />
          </Grid>
          <Hidden mdDown>
            <Grid item md={4} xs={6}>
              <BeforeAfterCard
                viewState={viewState}
                beforeImg={"/imgs/before-after/grill-single-2-before.jpg"}
                afterImg={"/imgs/before-after/grill-single-2-after.jpg"}
              />
            </Grid>
            <Grid item md={4} xs={6}>
              <BeforeAfterCard
                viewState={viewState}
                beforeImg={"/imgs/before-after/grill-single-3-before.jpg"}
                afterImg={"/imgs/before-after/grill-single-3-after.jpg"}
              />
            </Grid>
          </Hidden>
        </Grid>
      </Container>
    </Box>
  )
}
export default WorkSection
