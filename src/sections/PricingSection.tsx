import HeroPage from "@/components/HeroPage"
import { Box, Button, Container, Grid, Typography } from "@mui/material"
import Link from "next/link"
import React from "react"
import { type FC } from "react"
import StyledCard from "../components/StyledCard"

const PricingSection: FC = () => {
  return (
    <Box
      id="pricing"
      sx={{
        py: 10,
        backgroundColor: theme => theme.palette.background.paper
      }}
    >
      <Container maxWidth="lg">
        <Typography variant="h2" textAlign="center">Pricing</Typography>
        <Typography variant="body1" textAlign="center" mb={10}>To keep it simple, we have just two sets of pricing.</Typography>
        <Grid container spacing={2}>
          <Grid item md={6} xs={12}>
            <StyledCard
              sx={{
                bgcolor: theme => theme.palette.background.default
              }}
              src="/imgs/fredericksburg-bbq-party.jpg"
              alt="Free standing grill cleaning and repair services"
              title="Free Standing Grill"
              text={<>
                <Typography>For most consumer grills.</Typography>
                <Typography>Up to five burners.</Typography>
                <Typography>Price upgrade for each additional burner.</Typography>
              </>}
            />
          </Grid>
          <Grid item md={6} xs={12}>
            <StyledCard
              sx={{
                bgcolor: theme => theme.palette.background.default
              }}
              src="/imgs/grillfood2.jpg"
              alt="Built-in grill repair and cleaning"
              title="Built-In Grill"
              text={<>
                <Typography>For grills that are built into your property.</Typography>
                <Typography>Up to five burners.</Typography>
                <Typography>Price upgrade for each additional burner.</Typography>
              </>}
            />
          </Grid>
        </Grid>
      </Container>
      <Container maxWidth="lg" sx={{ mt: 5 }}>
        <HeroPage
          reverse
          imageHiddenMobile
          image="/imgs/fredericksburg-bbq-grime-removal.jpg"
          alt="Contact for a free quote"
        >
          <Typography variant="h2">
            Contact us for a free quote
          </Typography>
          <Typography variant="body1" mt={2}>
            {"Get in touch and receive a 100% free quote. We're proud to offer competitive pricing for all of our cleaning and maintenance services."}
          </Typography>
          <Link href="#contact">
            <Button
              sx={{
                mt: 2
              }}
              variant="contained"
            >
              Get a free quote
            </Button>
          </Link>
        </HeroPage>
      </Container>
    </Box>
  )
}
export default PricingSection
