import { Button, Container, MenuItem, Stack, TextField, Typography } from "@mui/material"
import { Box } from "@mui/system"
import React from "react"
import { type FC } from "react"
import HeroPage from "../components/HeroPage"

const ContactSection: FC = () => {
  return (
    <Container
      id="contact"
      maxWidth="lg"
      sx={{
        py: 10
      }}
    >
      <Typography variant="h2" textAlign="center">Contact us</Typography>
      <Typography variant="body1" textAlign="center">Get in touch with a professional, so you can get back to grilling.</Typography>
      <HeroPage
        py={5}
        image="/imgs/fredericksburg-bbq-chef-grilling.jpg"
        alt="Grill cleaning and maintenance"
        form
      >
        <Stack
          spacing={2}
          component="form"
          width="100%"
          action="/success"
        >
          <TextField
            fullWidth
            variant="filled"
            size="small"
            required
            label="Name"
            placeholder="Your name..."
            name="name"
            type="text"
          />
          <TextField
            fullWidth
            variant="filled"
            size="small"
            required
            label="Email"
            placeholder="Your email..."
            name="email"
            type="email"
          />
          <TextField
            fullWidth
            variant="filled"
            size="small"
            required
            label="Phone"
            placeholder="Your phone number..."
            name="phone"
            type="tel"
          />
          <TextField
            fullWidth
            variant="filled"
            size="small"
            required
            label="Address"
            placeholder="Your address..."
            name="address"
            type="text"
          />
          <TextField
            fullWidth
            variant="filled"
            size="small"
            required
            label="City"
            placeholder="Your city..."
            name="city"
            type="text"
          />
          <Stack
            spacing={2}
            direction="row"
          >
            <TextField
              fullWidth
              variant="filled"
              size="small"
              required
              label="State"
              placeholder="Your state..."
              name="state"
              type="text"
            />
            <TextField
              fullWidth
              variant="filled"
              size="small"
              required
              label="Zip"
              placeholder="Your zip code..."
              name="zip"
              type="text"
            />
          </Stack>
          <TextField
            select
            fullWidth
            variant="filled"
            size="small"
            label="Service"
            required
            name="inquiry"
            type="text"
            defaultValue="Cleaning"
          >
            <MenuItem value="Cleaning">Grill Cleaning</MenuItem>
            <MenuItem value="Repair">Grill Repair</MenuItem>
            <MenuItem value="Inspection">Grill Inspection</MenuItem>
          </TextField>
          <TextField
            multiline
            minRows={3}
            maxRows={3}
            fullWidth
            variant="filled"
            size="small"
            label="Message"
            placeholder="Let us know the details about your specific needs."
            name="form-message"
            type="text"
          />
          <Box>
            <Button
              variant="contained"
              type="submit"
              sx={{
                minWidth: "200px"
              }}
            >
              Submit
            </Button>
          </Box>
        </Stack>
      </HeroPage>
    </Container>
  )
}
export default ContactSection
